app.factory('$storageService', function() {
	return new (function () {
		this.get = function (name, defValue) {
			return window.localStorage[name] || defValue || null;
		};

		this.set = function (name, value) {
			window.localStorage[name] = value;
		};

		this.getOb = function (name, defValue) {
			$ob = JSON.parse(isset_r(window.localStorage[name], false));
			return $ob.length !== false ? $ob : defValue || false;
		};

		this.setOb = function (name, value) {
			window.localStorage[name] = JSON.stringify(value);
		};

		this.unset = function(name) {
			if(typeof window.localStorage[name] != "undefined")
				delete window.localStorage[name];
		};

		this.clear = function() {
			for(var i in window.localStorage){
				delete window.localStorage[i];
			}
		};
	});
});
