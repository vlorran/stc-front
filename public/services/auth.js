app.factory('$authService', ['$http', '$location', '$storageService', '$rootScope',
	function($http, $location, $storageService, $rootScope) {
	return new (function () {
		
		this.logged = false;
		this.id = null;
		this.access_token = null;
		this.expires_in = null;
		this.token_type = null;
		this.user = null;

		this.login = function(data){
			this.logged = true;
			this.access_token = data.access_token;
			this.expires_in = data.expires_in;
			this.token_type = data.token_type;
			this.id = data.id;
			$storageService.setOb('auth_data_d1',this.getUserData());
			this.getMyData();
		};

		this.logoff = function(){
			this.logged = false;
			this.id = null;
			this.access_token = null;
			this.expires_in = null;
			this.token_type = null;
			$storageService.clear();
			$location.path('/signin');
		};

		this.redirectToAppIfLogged = function(){
			if(this.logged){
				$location.path('/app');
			}
		};

		this.redirectToSignInIfNotLogged = function(){
			if(!this.logged){
				$location.path('/signin');
			}
		};

		this.getUserData = function(){
			return {
				logged: this.logged,
				id: this.id,
				access_token: this.access_token,
				expires_in: this.expires_in,
				token_type: this.token_type
			};
		};

		this.restApiGet = function(path, callback){
			hide_loading(true);
			this.restApi($http.get(path_api+path+'?access_token='+this.access_token), callback)
		};

		this.restApiGetWithData = function(path, data, callback){
			hide_loading(true);
			var url_append = "";
			for(var i in data){
				url_append = url_append+"&"+i+"="+data[i];
			}
			this.restApi($http.get(path_api+path+'?access_token='+this.access_token+url_append), callback)
		};

		this.restApiPost = function(path, data, callback){
			hide_loading(true);
			data['access_token'] = this.access_token;
			this.restApi($http.post(path_api+path, data), callback)
		};

		this.restApi = function(ob, callback){
			var ajax = ob.then(function(d){
				hide_loading(false);
				callback(d);
			},function(data){
				hide_loading(false);
			});
		};

		this.addContact = function(number, callback){
			this.restApiPost('add-contact',{number:number},callback);
		};

		this.getContacts = function(callback){
			this.restApiGet('contacts',callback);
		};

		this.getMessages = function(user_id, callback){
			this.restApiGetWithData('messages', { user_id:user_id }, callback);
		};

		this.getMyData = function(){
			this.restApiGet('my-data',function(d){
				if(typeof d.data.user != 'undefined'){
					this.user = d.data.user;
					$rootScope.getSocketService().emit('set_me', {id:this.user.id});
				}
			});
		}

	    var auth_data = $storageService.getOb('auth_data_d1', false);

	    if (auth_data !== false) {
			this.logged = true;
			this.id = auth_data.id;
			this.access_token = auth_data.access_token;
			this.expires_in = auth_data.expires_in;
			this.token_type = auth_data.token_type;
			this.getMyData();
	    }
	});
}]);

