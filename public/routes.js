app.config(function($routeProvider){
	$routeProvider.when("/",{
		templateUrl: "/views/index.html",
		controller: "AppCtrl"
	})
	.when("/signup",{
		templateUrl: "/views/signup.html",
		controller: "SignUpCtrl"
	})
	.when("/signin",{
		templateUrl: "/views/signin.html",
		controller: "SignInCtrl"
	})
	.when("/app",{
		templateUrl: "/views/app.html",
		controller: "AppCtrl"
	})
	.when("/app/message/:phone",{
		templateUrl: "/views/app.message.html",
		controller: "AppMessageCtrl"
	})
	.otherwise({
		redirectTo:'/'
	});
});