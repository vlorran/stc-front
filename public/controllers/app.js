app.controller('AppCtrl', function($scope, $location, $authService, $socketService, $rootScope) {
	$authService.redirectToSignInIfNotLogged();
	$rootScope.update_contacts();

	$scope.add_contact = function(){
		$scope.error = "";
		if($scope.number){
			$authService.addContact($scope.number, function (e){
				var data = e.data;
				if(typeof data.error != "undefined"){
					$scope.error = data.error;
					return;
				}
				$socketService.emit('added-user', {id:data.id});
				$rootScope.update_contacts();
			});
		}else{
			$scope.error = "Type a number!"
		}
	};

	$scope.message = function(number){
		$scope.error = "";
		if(number){
			var user = $scope.findContactByNumber(number);
			if(user === false){
				$scope.error = "User not found! Click in 'Add Contact' button.";
			}else{
				$scope.user_selected = user;
				$location.path('/app/message/'+user.phone);
			}
		}else{
			$scope.error = "Type a number!";
		}
	};

});