app.controller('AppMessageCtrl', function($scope, $location, $http, $authService, $routeParams, $socketService, $rootScope) {
	var user = $scope.findContactByNumber($routeParams.phone);
	if(user == false){
		$scope.error = "User not found with number "+$routeParams.phone;
		url_after_load = "/app/message/"+$routeParams.phone;
		$location.path('/app');
		return;
	}

	var old_messages = [];
	for(var i in $rootScope.notread){
		if($rootScope.notread[i].from_user_id != user.id){
			old_messages.push($rootScope.notread[i]);
		};
	}
	$rootScope.notread = old_messages;

	$rootScope.appendMessageIfUser = function(message){
		if(message.from_user_id == $scope.contact.id){
			$scope.messages.push(message);
			$scope.$apply();
			setTimeout(function(){
				$(".message-scroller").animate({ scrollTop: $('.message-scroller').height()+10000 }, 200);
			},10);
			return true;
		}

		return false;
	};

	$scope.contact = user;

	$scope.messages = [];

	$scope.my_id = null;

	$authService.getMessages(user.id,function(d){
		var data = d.data;
		if(typeof data.messages != "undefined"){
			$scope.messages = data.messages;
		}
		if(typeof data.my_id != "undefined"){
			$scope.my_id = parseInt(data.my_id);
		}
	});

	setTimeout(function(){
		$(".message-scroller").animate({ scrollTop: $('.message-scroller').height()+10000 }, 200);
	},500);


	$scope.send_message = function(){
		if($scope.message){
			$socketService.emit('new-message',{
				user_id:user.id,
				access_token:$authService.access_token,
				message:$scope.message
			}, function(data){
				if(typeof data.message != 'undefined'){
					$scope.messages.push(data);
					$scope.$apply();
					setTimeout(function(){
						$(".message-scroller").animate({ scrollTop: $('.message-scroller').height()+10000 }, 200);
					},10);
				}
			});
			$scope.message = "";
		}
	};
});