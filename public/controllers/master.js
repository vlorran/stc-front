app.controller('ControllerMaster', function($scope, $location, $authService, $socketService, $rootScope) {
	$rootScope.online = false;
	$scope.go_to_login = function (){
		$location.path('/signin');
	};
	$scope.go_to_signup = function (){
		$location.path('/signup');
	};
	$scope.go_to_home = function (){
		$location.path('/');
	};

	$scope.go_to_app = function(){
		$location.path('/app');
	};

	$rootScope.notread = [];
	
	$scope.is_logged = $authService.logged;


	$rootScope.contact_users = [];
	$scope.user_selected = [];

	$rootScope.update_contacts = function(){
		$authService.getContacts(function (e){
			var data = e.data;
			if(typeof data.error != "undefined"){
				$rootScope.error = data.error;
			}
			
			if(typeof data.users != "undefined"){
				$rootScope.contact_users = data.users;
			}
			if(url_after_load){
				$location.path(url_after_load);
				url_after_load = "";
			}
		});
	};
	$socketService.on('in-message',function(data){
		if($rootScope.appendMessageIfUser && $rootScope.appendMessageIfUser(data)){
			return true;
		}else{
			$rootScope.notread.push(data);
			$rootScope.$apply();
		}
	});

	$socketService.on('added-me',function(data){
		$rootScope.update_contacts();
	});

	$rootScope.getSocketService = function(){
		return $socketService;
	};

	if($scope.is_logged){
		$rootScope.update_contacts();
	}

	$socketService.on('connect',function(){
		$rootScope.online = true;
		$rootScope.$apply();
	});
	$socketService.on('disconnect',function(){
		$rootScope.online = false;
		$rootScope.$apply();
	});

	$scope.findContactByNumber = function(number){
		for(var i in $rootScope.contact_users){
			if($rootScope.contact_users[i].phone == number){
				return $rootScope.contact_users[i];
			}
		}
		return false;
	};
});