app.controller('SignUpCtrl', function($scope, $location, $http, $authService) {
	$authService.redirectToAppIfLogged();
	$scope.errors = [];
	$scope.send_form = function(){
		hide_loading(true);
		var ajax = $http.post(path_api+"register", $scope.user).then(function(data){
			hide_loading(false);
			var res = data.data;
			if(typeof res.errors != "undefined"){
				$scope.errors = res.errors;
				return;
			}
			if(typeof res.saved != "undefined"){
				just_saved = true;
				$location.path('/signin');
				return;
			}
		},function(data){
			hide_loading(false);
			$scope.errors = ['Unknow error.'];
		});
	};
})