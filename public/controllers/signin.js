app.controller('SignInCtrl', function($scope, $location, $http, $authService) {
	$authService.redirectToAppIfLogged();

	$scope.errors = [];
	$scope.just_saved = false;
	if(just_saved){
		$scope.just_saved = true;
		just_saved = false;
	}
	$scope.signin = function(){
		hide_loading(true);
		var ajax = $http.post("/signin", $scope.user).then(function(data){
			hide_loading(false);
			$scope.errors = [];
			var res = data.data;
			if(typeof res.error_description != "undefined"){
				$scope.errors = [res.error_description];
				return;
			}
			if(typeof res.access_token != "undefined"){
				$authService.login(res);
				$location.path('/app');
				return;
			}
		},function(data){
			hide_loading(false);
			$scope.errors = ['Unknow error.'];
		});
	};
});