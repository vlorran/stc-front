var path_api = "http://stc.datayet.com/";
var just_saved = false;
var url_after_load = "";
var app = angular.module('speedContact', ['ngRoute']);
var socket_id = null;

function isset_r($ob, $def){
    $v = $def || false;
    if(typeof $ob != "undefined"){
        return $ob;
    }
    return $v;
}

function hide_loading(ob){
	if(ob){
		$('.sp-loading').fadeIn();
	}else{
		$('.sp-loading').fadeOut();
	}
}

(function(){
	hide_loading(false);
})();
