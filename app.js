var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var request = require('request');

var path_api = "http://stc.datayet.com/";

var credentials = {
	grant_type:'password',
	client_id:'teste1',
	client_secret:'teste'
};

app.listen(3000, function () {

});

app.use(express.static(__dirname + '/public'));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
	extended: true
}));

app.get('/', function(req, res) {
	res.render('index');
});


app.post('/signin', function(req, res) {
	var data = credentials;
	data["username"] = req.body.email;
	data["password"] = req.body.password;
	request.post(path_api+'oauth/access_token',
		{
			form: data
		},
		function (error, response, body) {
			res.send(body);
			return;
		}
	);
});
